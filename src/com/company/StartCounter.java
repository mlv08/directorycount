package com.company;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final  class StartCounter {
     public StartCounter() {

    }
    public static Stream<File> streamMaster(Stream<Path> currentPath) throws IOException {
          return  currentPath.map(Path::toFile)
                   .flatMap(file -> {
                       try {
                           return file.isDirectory() ?
                                   Stream.concat(Stream.of(file), streamMaster(Files.list(Paths.get(file.getAbsolutePath())))) :
                                   Stream.of(file);
                       } catch (IOException e) {
                           e.printStackTrace();
                           return null;
                       }
                   });

    }
    public static Map<String,Long> counterFilesDirectory(Stream<Path> currentpath) throws IOException {

        Map<String, Long> result = streamMaster(currentpath)
                .map(StartCounter::getExtension)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        System.out.println(result);
        return result;
    }
    public static String getExtension(File file){
        if (file.isDirectory()) return "Directory";
        else if (file.isHidden() && file.isFile()) return "Hidden File";
        else return file.getName().contains(".") ?
                file.getName().substring(file.getName().lastIndexOf(".") + 1) :
                "no ext ";
    }
    public  void inputPath(){
        System.out.println("Inserire un percorso valido: ");
            try {
                 Path s = Paths.get(new Scanner(System.in).nextLine());
                if(s.toFile().canRead() && s.toFile().isDirectory()  ) {
                    System.out.println();
                    streamMaster(Files.list(s));
                    counterFilesDirectory(Files.list(s));
                }
                else{
                    System.out.println("Questo Path non esiste");
                    inputPath();
                }
            } catch (InvalidPathException | IOException e) {
                System.out.println("Questo path non è valido");
                inputPath();
            }
    }

}
